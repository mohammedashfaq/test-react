import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


it("add", () => {
 let a = 2
 let b = 3
 let c = a + b
 expect(c).toBe(4)
})
